# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/sii/sii-51144/practica1/src/Esfera.cpp" "/home/sii/sii-51144/practica1/build/src/CMakeFiles/bot.dir/Esfera.cpp.o"
  "/home/sii/sii-51144/practica1/src/Mundo.cpp" "/home/sii/sii-51144/practica1/build/src/CMakeFiles/bot.dir/Mundo.cpp.o"
  "/home/sii/sii-51144/practica1/src/Plano.cpp" "/home/sii/sii-51144/practica1/build/src/CMakeFiles/bot.dir/Plano.cpp.o"
  "/home/sii/sii-51144/practica1/src/Raqueta.cpp" "/home/sii/sii-51144/practica1/build/src/CMakeFiles/bot.dir/Raqueta.cpp.o"
  "/home/sii/sii-51144/practica1/src/Vector2D.cpp" "/home/sii/sii-51144/practica1/build/src/CMakeFiles/bot.dir/Vector2D.cpp.o"
  "/home/sii/sii-51144/practica1/src/bot.cpp" "/home/sii/sii-51144/practica1/build/src/CMakeFiles/bot.dir/bot.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
